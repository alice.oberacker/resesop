import argparse
import sys
import os
from joblib import Parallel, delayed
import numpy
from datetime import datetime
from os.path import abspath
from os import pardir
sys.path.append(abspath(pardir + "/src"))

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from Resesop import Resesop


class ResesopParallel(Resesop):

    def __init__(self, path, experiment_id, nb_iterations, geometry:str, discrete='beta',
                 eta_type='', verbose=0, start_id=None, nb_samples=1, ids=None, device='cuda'):
        """
        Initialise ResesopParallel using Resesop. Eta is now loaded from model predictions, RESESOP is applied to
        nb_samples many samples in parallel.

        :param path: Data Path
        :param experiment_id: Experiment ID
        :param nb_samples: Number of samples to backpropagate
        :param start_id: First id to process
        :param nb_iterations: Maximum number of iterations for backpropagation
        :param geometry: which CT geometry should be used: fan or parallel
        :param discrete: in which direction eta is discretised
        :param eta_type: Name of model
        """
        assert geometry in ['fan', 'parallel'], f'Invalid geometry: {geometry}.'
        super().__init__(path, experiment_id, geometry, device)
        # set up log_debug, so it is available in the current thread
        self.log_info = self.utils.set_up_logging(level='info')
        # self.log_debug = self.utils.set_up_logging(level='debug')

        self.log_info.info(f"Load sinograms: {os.path.join(self.utils.sinogram_folder, f'sinogram_db.h5')}")
        if ids is not None:
            self.sinogram_p_set, self.ids = self.utils.load_sinogram_h5(ids=ids)
        elif start_id is not None:
            self.sinogram_p_set, self.ids = self.utils.load_sinogram_h5(ids=range(start_id, start_id+nb_samples))
        else:
            raise ValueError('Either ids or start_id must be not None.')

        self.log_info.info(f'Ids to run: {self.ids}')
        self.log_info.info(f"Load phantoms: {os.path.join(self.utils.phantom_folder, f'phantoms_db.h5')}")
        self.phantom_set = self.utils.load_phantom_h5(ids=self.ids)
        self.eta_type = eta_type
        self.discrete = discrete
        self.verbose = verbose

        # if the eta_type is 'calculated' use the calculated eta for backprojection
        if self.eta_type == 'calculated':
            # load calculated etas
            self.log_info.info('Load calculated eta')
            self.eta_set = self.utils.load_eta_h5(discrete=discrete, ids=self.ids)
        elif self.eta_type == 'zero':
            self.log_info.info('Load zero eta')
        else:
            raise ValueError(f'Invalid eta value: {self.eta_type}.')

        # number of samples to backpropagate
        self.nb_samples = nb_samples if nb_samples is not None else len(self.sinogram_p)
        # maximum number of iterations
        self.iterations = nb_iterations

    def get_sample(self, index):
        """
        Get sinogram, phantom and eta for a given sample index.
        If no model is provided we return eta = 0.

        :param index: Index of wanted sample
        :return: sinogram, phantom, eta
        """
        sino_p = self.sinogram_p_set[index]
        phantom = self.phantom_set[index]
        id = self.ids[index]
        if self.eta_type == 'calculated':
            eta_error = self.eta_set[index, :]
            # approximation error from NN
            eta = numpy.repeat(eta_error[numpy.newaxis, ...], self.M, axis=0)
        else:
            # eta = numpy.zeros((self.M, self.setup.K))
            eta = numpy.random.uniform(low=0, high=0.0001, size=(self.M, self.setup.K))
        return sino_p, phantom, eta, id

    def run_resesop_parallel(self, sample):
        """
        Run RESESOP for a single sample using the given eta.

        :param sample: id of sample to backpropagate
        :return:
        """
        # set up log_debug and log_info, so it is available in the current thread
        self.log_debug = self.utils.set_up_logging(level='debug')
        self.log_info = self.utils.set_up_logging(level='info')

        self.log_debug.debug(f'run resesop, sinogram id {sample}')
        # get sinogram, phantom and predicted eta for this sample
        try:
            self.sinogram_p, self.phantom, self.eta, self.id = self.get_sample(sample)
        except IndexError:
            return
        # plot phantom and sinogram next to each other
        self.utils.plot_two(self.phantom, self.sinogram_p, a_name='phantom', b_name='sinogram', close=1
                            , save=os.path.join(self.utils.result_folder, f'goal_sample_{self.id}.png'))

        # start resesop
        f, errors, _ = self.run_resesop(self.iterations, two_directions=True, verbose=0, sample=self.id)

        # save backprojection
        self.utils.save_back_prop(f, suffix=self.id)

        if self.verbose > 0:
            # plot final f and what it should look like
            eta_str = '_calculated_eta' if self.eta_type == 'calculated' \
                else self.eta_type if self.eta_type else '_zero_eta'
            plot_path = os.path.join(self.utils.result_folder, f'final_backprojection_sample_{self.id}{eta_str}.png')
            self.utils.plot_two(f, self.phantom, a_name='backprojection', b_name='original'
                                , title=f'Final backprojection', close=0
                                , save=plot_path)
            # plot errors for each iteration
            self.utils.plot_errors(errors, name=f'_sample_{self.id}{eta_str}', close=0)
            # self.utils.plot_errors_log(errors, name=f'_sample_log_{self.id}{eta_str}', close=0)

        # final different between estimated phantom and actual phantom
        log_str = 'Errors of f - phantom for sample {}, id {}, iterations {}: ' \
                  'L1 {:.2f}, L2 {:.2f} MSE {:.2f}, '\
                  'PSNR {:.2f}, SSIM {:.2f}'.format(sample,
                                              self.id,
                                              self.iterations,
                                              *errors[-1])
        self.log_info.info(log_str)

    @staticmethod
    def parallel(n_cores, function, jobs):
        """

        :param n_cores: Number of cores to use
        :param function: Function to parallelise
        :param jobs: List of tuples with (id, path to data folder)
        :return:
        """
        _ = Parallel(n_jobs=n_cores)(delayed(function)(sample) for sample in jobs)

    def run_jobs(self, numb_cores, function, job_list):
        # parallelise the execution of all jobs in job_list
        self.parallel(numb_cores, function, job_list)

    def create_jobs(self):
        """
        Create a list of length nb_samples for parallelization.
        :return:
        """
        list_samples = [s for s in range(self.nb_samples)]
        return list_samples


def read_args():
    """
    Read execution parameters to generate
    --path, -p path to data directory
    --experiment_id, -e id of experiment to execute
    --cores, -c number of cores
    --model, -m model to use for eta: calculated, name of model/predictions file, empty string for eta=0
    --iterations, -i number of iterations to execute
    --samples, -s number of samples
    --start_id first sample id to use

    Examples:
        * $ run_parallel.py -c 4 -s 2000 -p C:Users\...
        * $ run_parallel.py --cores 4 --samples 2000 --path C:Users\...
    :return: parsed arguments
    """
    parser = argparse.ArgumentParser(description='Process inputs for resesop backprojections.')
    parser.add_argument('--path', '-p', type=str, help='the path to the data directory.')
    parser.add_argument('--experiment_id', '-e', type=str, help='id of experiment to execute.')
    parser.add_argument('--device', '-d', type=str, default='cpu', help='cuda or cpu.')
    parser.add_argument('--geometry', '-g', type=str, help='type of geometry: fan or parallel.')
    parser.add_argument('--cores', '-c', type=int, default=1, help='number of cores to use.')
    parser.add_argument('--eta', type=str, default='zero', help='calculated or zero.')
    parser.add_argument('--iterations', '-i', type=int, default=20, help='number of iterations to execute.')
    parser.add_argument('--samples', '-s', type=int, default=20, help='number of samples to run.')
    parser.add_argument('--start_id', type=int, default=0, help='first sample id to run.')

    arguments = parser.parse_args()
    return arguments


if __name__ == "__main__":
    """
    Running RESESOP algorithm with -s number of samples for experiment -e and -i many iterations.
    When multiple samples should be backpropagated, they will be run in parallel.
    
    When model name -m is empty use eta=0,
    when model name -m is 'calculated' use the calculated eta: |perturbed sinogram - original sinogram|,
    when model name -m is a model id then use the predictions from that model as eta.    
    """

    args = read_args()
    # if running specific id, change job_list
    job_list = None
    start_id = None
    # if no job list provided start with the first sample
    if job_list is None:
        start_id = 0
    rp = ResesopParallel(path=args.path,
                         experiment_id=args.experiment_id,
                         nb_samples=args.samples,
                         start_id=start_id,
                         ids=job_list,
                         nb_iterations=args.iterations,
                         geometry=args.geometry,
                         discrete='beta',
                         eta_type=args.eta,
                         device=args.device,
                         verbose=1)

    # set up logging
    logger = rp.utils.set_up_logging(level='debug', filename='resesop')
    logger_info = rp.utils.set_up_logging(level='info', filename='resesop')
    logger_info.info(f'experiment#{args.experiment_id}, geometry is {args.geometry}, eta is {args.eta}, iterations {args.iterations}')

    # create jobs
    job_list = rp.create_jobs()
    start_t = datetime.now()
    # start executing in parallel
    rp.run_jobs(args.cores, rp.run_resesop_parallel, job_list)
    end = datetime.now()

    logger_info.info('#' * 10)
    logger_info.info(f'{args.iterations} iterations took {(end - start_t).total_seconds()}')
    logger_info.info('#' * 10)
