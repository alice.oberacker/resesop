import math
import os
import sys
import numpy
from skimage.metrics import structural_similarity
from datetime import datetime
from scipy.sparse import csr_matrix
import torch

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from gendata.Sinogram import Sinogram
from gendata.Setup_parallel import SetupParallel
from gendata.Setup_fanflat import SetupFanFlatASTRA as SetupFan
from resesop.UtilsResesop import Utils


class Resesop:

    def __init__(self, path, experiment_id, geometry='fan', device='cuda'):
        if geometry == 'fan':
            self.setup = SetupFan(experiment_id)
        elif geometry == 'parallel':
            self.setup = SetupParallel(experiment_id)
        self.utils = Utils(path, f'experiment#{experiment_id}')

        # # Number detector points
        self.M = self.setup.P * 2 + 1
        # #  size of phantom
        self.N = self.setup.R * 2 + 1

        self.dy = 2 / self.M
        # error terms
        self.B = 1
        # error in data
        self.delta = numpy.ones((self.M, self.setup.K)) * 0.001
        self.tau = 1.00001

        self.phantom = None
        self.sinogram = None
        self.sinogram_p = None
        self.eta = None
        self.log_debug = self.utils.set_up_logging(level='debug', filename='resesop')

        # set device for matrix computations
        self.device = torch.device('cuda' if (torch.cuda.is_available() and device == 'cuda') else 'cpu')
        # removed
        # self.device = 'cpu'
        self.log_debug.debug(f'using {self.device} for matrix computations.')

        # load weights
        weight_path = self.utils.weight_path.format(self.setup.K, self.setup.P, self.setup.R) + '.npz'
        self.weights = self.utils.load_npz_weights_tensor(weight_path,
                                                          fov_shape=(self.N, self.N),
                                                          num_angles=self.setup.K,
                                                          num_detectors=self.M,
                                                          device=self.device)

    def generate_sinogram(self, phantom, phi_index=None, detector_index=None):
        """
        generate Sinograms
        :param phantom: Phantom to use for sinogram
        :param phi_index: angle index at which to calculate sinogram
        :param detector_index: detector index at which to calculate sinogram
        :return:
        """
        gen_sinogram = Sinogram(phantom=phantom, nb_source_angles=self.setup.K, angle_index=phi_index,
                                detector_index=detector_index, angle_ray_weights=self.weights, utils=self.utils)
        gen_sinogram.generate_sinograms()
        sinogram = gen_sinogram.sinogram

        # if we're calculating the sinogram for one specific ray,
        # we need to return just a float instead of list of float
        if detector_index is not None and phi_index is not None:
            sinogram = sinogram[0]
        return sinogram, gen_sinogram.angle_ray_weights

    def calculate_diff_sinos(self, phantom, sinogram, phi_idx, det_idx, weight):
        """
        Calculate Af - g: the difference between the sinogram of the estimated phantom and the
        real sinogram for one specific sensor angle and detector point.
        :param phantom: Current phantom estimation f
        :param sinogram: Given sinogram
        :param phi_idx: Index of angle
        :param det_idx: Index of detector
        :return:
        """
        # calculate Sinogram of current phantom estimate f (only for given angle and detector point)
        new_sinogram, weights = self.generate_sinogram(phantom, phi_index=phi_idx, detector_index=det_idx)
        # assert not numpy.isnan(new_sinogram).any()
        # assert not numpy.isnan(new_sinogram)

        # calculate Af - g
        w = new_sinogram - sinogram[det_idx, phi_idx]
        return w, new_sinogram, weights

    def two_search_directions(self, projection, u_o, u_n, alpha, xi, s_prod_un, s_prod_uo):
        """
        Calculate the new phantom using two search directions.
        :param projection:
        :param u_o:
        :param u_n:
        :param alpha:
        :param xi:
        :param s_prod_un:
        :param s_prod_uo:
        :return:
        """
        # <u_old, fn>
        in_prod_uo_fn = self.discrete_prod(u_o, projection)

        # <u_new, u_old>
        in_prod_un_uo = self.discrete_prod(u_n, u_o)

        # check on which side of the stripe the projection is on
        # H_ >/</= (u_o, alpha_old +/- xi_old)
        t = self.test_projection_location(alpha, xi, in_prod_uo_fn, s_prod_un, s_prod_uo, in_prod_un_uo)

        # f_n+1 = f~_n+1 - t1 * u_new - t2 * u_old
        f_n = self.calculate_f(projection, t, s_prod_un, in_prod_un_uo, u_o, u_n)
        return f_n

    def get_weights(self, phi_index):
        """
        weights are of shape (number angles, number detectors, res, res)
        :param phi_index:
        :return:
        """
        weights = self.weights[phi_index]
        return weights

    def run_resesop(self, max_iterations, two_directions=True, verbose=0, sample=None):
        # set up logging
        # self.log_debug = self.utils.set_up_logging(level='debug')

        # define start variables
        # f_0 = numpy.zeros((self.N, self.N))
        f_0 = torch.zeros((self.N, self.N)).to(device=self.device)
        # u_old = numpy.zeros((self.N, self.N))
        u_old = torch.zeros((self.N, self.N)).to(device=self.device)
        # g = torch.from_numpy(self.sinogram_p).to(device=self.device)
        g = torch.tensor(self.sinogram_p).to(device=self.device)

        # for parallel execution only
        sample_id = f' sample {sample}' if sample is not None else ''

        errors = []
        f = f_0
        alpha_old = 0
        xi_old = 0

        n = 0
        # iterate until stopping criteria satisfied or maximum number of iterations reached
        stopped = numpy.ones(g.shape)
        first_iter = True
        while sum(sum(stopped)) > 0 and n <= max_iterations-1:
            self.log_debug.debug(f'Sample {sample}, iteration: {n}')
            start_iter = datetime.now()

            # iterate over scanner positions
            for phii, phi in enumerate(self.setup.ANGLES):
                weights = self.get_weights(phii)

                if verbose >= 4:
                    self.utils.plot_two(f, self.phantom, a_name='backprojection', b_name='original',
                                        title=f'phi: {phii - 1}', close=2)

                # iterate over detector points
                for deti in range(self.M):
                    if verbose >= 1:
                        self.log_debug.debug(f'phi: {phii}, det: {deti}')
                    # calculate Sinogram of current phantom estimate f (only for given beta angle)
                    # and calculate Af - g
                    res, new_sino, weights_ = self.calculate_diff_sinos(f, g, phii, deti, weights[deti])

                    eta_delta = self.eta[deti, phii] + self.delta[deti, phii]
                    # Calculate bounds and stop_criterion = (abs(res[:, bi]) > tau * (eta[:, bi] + delta[:, bi]))
                    res, index = self.test_discrepancy(res, self.tau, eta_delta)

                    # check if discrepancy principle is satisfied
                    # if index.astype(int) == 0:
                    if not torch.any(index):

                        # discrepancy principle satisfied for all detector points for this phi
                        if verbose >= 4:
                            self.log_debug.debug(f'discrepancy principle satisfied for detector point {deti} and phi {phii}.')
                        # remember that it is satisfied for current phi
                        stopped[deti, phii] = 0
                    else:

                        alpha, xi = self.calculate_alpha_xi(res, g[deti, phii], eta_delta)

                        # calculate adjunct
                        # u_new = self.adjunct(res, self.N, csr_matrix(weights[deti]))
                        u_new = self.adjunct(res, self.N, weights[deti])

                        # u_new is not zero, would be 0 if detector deti does not absorb anything
                        # if u_new.count_nonzero() != 0:
                        # if torch.sum(u_new) != 0:
                        if torch.sparse.sum(u_new) != 0:
                            # ||u_n||^2
                            in_prod_un_un = self.discrete_prod(u_new, u_new)
                            # calculate f^~
                            fn = self.calculate_f_tilde(f, abs(res), eta_delta, u_new, in_prod_un_un)
                            # fn = self.calculate_f_tilde(f, abs(res), self.delta[deti, phii] - self.eta[deti, phii],
                            #                             u_new, in_prod_un_un)

                            if not two_directions:
                                # only one search direction:
                                f = fn
                            else:
                                # very first iteration
                                # u_old, etc does not exist yet.
                                if first_iter:
                                    f = fn
                                    first_iter = False

                                # every next iteration
                                else:
                                    # use both search directions
                                    f = self.two_search_directions(fn, u_old, u_new, alpha_old, xi_old, in_prod_un_un,
                                                                   in_prod_uo_uo)
                                    # if numpy.isnan(f).any():
                                    if torch.any(torch.isnan(f)):
                                        self.log_debug.debug(f'nan values in iteration phi {phii}, det {deti}.')
                                        break

                                # remember variables for next iteration
                                u_old = u_new
                                alpha_old = alpha
                                xi_old = xi
                                in_prod_uo_uo = in_prod_un_un

                    if verbose >= 4:
                        self.utils.plot_two(f, self.phantom, a_name='backprojection', b_name='original',
                                            title=f'phi: {phii}, detector: {deti}')

                    # set all negative values to 0
                    # f = numpy.multiply(f, f > 0)
                    f = torch.multiply(f, f > 0)

            # convert f from numpy matrix to numpy array
            # f_ = numpy.asarray(f)
            f_ = f.cpu().numpy()
            # calculate an error between current f and the phantom we're looking for
            error = self.calc_error(self.phantom, f_)
            errors.append(error)

            # save intermediate back projection for debugging only
            # self.utils.save_back_prop(f_, suffix=f"iteration_{n}{sample_id.replace(' ', '_')}")

            # plot current backprojection next to the desired phantom to evaluate progress
            if verbose >= 2:
                file_name = f"Iteration_{n}{sample_id.replace(' ', '_')}_backprojection.png"
                self.utils.plot_two(f_, self.phantom, a_name='backprojection', b_name='original'
                                    , title=f'Iteration {n} backprojection', close=2
                                    , save=os.path.join(self.utils.result_folder
                                                        , file_name))
            self.log_debug.debug(f'Iteration {n} for {sample_id} took {(datetime.now() - start_iter).total_seconds()} seconds.')
            n += 1
        return f_, numpy.array(errors), stopped

    @staticmethod
    def calculate_alpha_xi(w, g, eta_delta_error):
        """
        Calculate alpha and xi values as
        alpha = <w_detector_angle; g_detector_angle>
        xi = (delta + eta) * ||w_detector_angle||
        :param w: (estimated sinogram - given sinogram) at specific detector and angle
        :param g: sinogram at specific detector and anlge
        :param eta_delta_error: error calculated from eta + delta
        :return:
        """
        a = w * g
        x = eta_delta_error * abs(w)
        # a = numpy.sum(a)
        # x = numpy.sum(x)

        return a, x

    @staticmethod
    def adjunct(w, resolution, weights):
        return resolution ** 2 / 4 * weights * w

    @staticmethod
    def calculate_f_tilde(phantom, w, eta_delta_error, adjunct, s_prod_u):
        sum_ = w * (w - eta_delta_error)
        f_n = phantom - (sum_ * adjunct / s_prod_u)
        # transform numpy.matrix back to numpy array
        # f_n = numpy.squeeze(numpy.asarray(f_n))
        return f_n

    @staticmethod
    def calculate_f(phantom, t, s_prod_un, s_prod_un_uo, u_o, u_n):
        t1 = - s_prod_un_uo * t
        t2 = s_prod_un * t
        # f_n+1 = f~_n+1 - t1 * u_new - t2 * u_old
        f_n = phantom - t1 * u_n - t2 * u_o
        return f_n

    @staticmethod
    def test_discrepancy(w, tau, eta_delta_error):
        """
        Discrepancy principle: ||Af - g|| > tau * (eta*B + delta)
        Return w if error is larger than Af - g, 0 if it is smaller.
        :param w: Af - g for a single angle and detector point
        :param tau:
        :param eta_delta_error:
        :return:
        """
        idx = abs(w) > tau * eta_delta_error
        w = w * idx
        return w, idx

    @staticmethod
    def test_projection_location(alpha, xi, s_prod_uo_fn, s_prod_un, s_prod_uo, s_prod_un_uo):
        """
        Find out on which side of the stripe the projection is on.
        :param alpha: parameter of stripe
        :param xi: parameter of stripe
        :param s_prod_uo_fn: <u_old, f_n>
        :param s_prod_un: <u_new, u_new>
        :param s_prod_uo: <u_old, u_old>
        :param s_prod_un_uo: <u_new, u_old>
        :return:
        """
        # make sure we're not dividing by 0
        if s_prod_un * s_prod_uo == s_prod_un_uo ** 2:
            return 0

        # if on one side of stripe
        # H_>(u_o, alpha_old + xi_old)
        if s_prod_uo_fn > alpha + xi:
            t = (s_prod_uo_fn - (alpha + xi))
            t /= (s_prod_un * s_prod_uo - s_prod_un_uo ** 2)

        # # if on other side of stripe
        # # H_<(u_o, alpha_old - xi_old)
        elif s_prod_uo_fn < alpha - xi:
            t = (s_prod_uo_fn - (alpha - xi))
            t /= (s_prod_un * s_prod_uo - s_prod_un_uo ** 2)

        # if already on stripe
        else:
            t = 0

        return t

    def calc_error(self, x, y):
        x = numpy.asarray(x)
        y = numpy.asarray(y)
        l1 = self.calc_l1(x, y)
        l2 = self.calc_l2(x, y)
        mse = self.calc_mse(x, y)
        psnr = self.calc_psnr(x, y)
        ssim_ = self.calc_ssim(x, y)
        return [l1, l2, mse, psnr, ssim_]

    @staticmethod
    def calc_mse(x, y):
        """
        Calculate the mean squared error of x and y: ||x - y|| = 2/size * mean((x-y)**2))
        :param x:
        :param y:
        :return:
        """
        norm = numpy.mean((x - y) ** 2)
        return norm

    @staticmethod
    def calc_ssim(x, y):
        return structural_similarity(x, y, data_range=y.max() - y.min(),
                                     win_size=None, gradient=False, multichannel=False, gaussian_weights=False,
                                     full=False)

    @staticmethod
    def ssim_discrete(x, y, axis=1):
        if axis == 0:
            # along rows
            x_ = x.T
            y_ = y.T
        elif axis == 1:
            # along columns
            x_ = x
            y_ = y

        ssims = []
        for xi, yi in zip(x_, y_):
            ssims.append(structural_similarity(xi, yi, data_range=1))
        return ssims

    @staticmethod
    def calc_psnr(x, y):
        mse = numpy.mean((x - y) ** 2)
        if (mse < 10e-324):  # MSE is zero means no noise is present in the signal .
            # Therefore PSNR have no importance.
            return 100
        max_pixel = 1.0
        psnr = 20 * math.log10(max_pixel / math.sqrt(mse))
        return psnr

    @staticmethod
    def calc_l2(x, y):
        """
        Calculate the L2 norm error of x and y: ||x - y||_L2 = sqrt(sum(sum((x-y)**2)))
        :param x:
        :param y:
        :return:
        """
        x = numpy.asarray(x - y)
        x = sum(sum(x ** 2))
        norm = math.sqrt(x)
        return norm

    @staticmethod
    def calc_l1(x,y):
        """
        Calculate the L1 norm error of x and y: ||x - y||_L2 = 2/size * sum(sum(x-y))
        :param x:
        :param y:
        :return:
        """
        x = numpy.asarray(x - y)
        norm = sum(sum(abs(x)))
        return norm


    @staticmethod
    def discrete_norm(x):
        """
        Discrete norm of vector x: 2/size * sqrt(sum(x**2))
        :param x:
        :return:
        """
        h = 2 / len(x)
        norm = h * math.sqrt(sum(x ** 2))
        return norm

    @staticmethod
    def discrete_prod(x, y):
        """
        discrete product of matrix x and y: 4/len**2 * sum(x*.y)
        :param x:
        :param y:
        :return:
        """
        h = 2 / x.shape[0]
        # make x and y sparse when they are not, so we can multiply them
        if not x.is_sparse: x = x.to_sparse()
        if not y.is_sparse: y = y.to_sparse()
        prod = h ** 2 * torch.sparse.sum(torch.multiply(x, y))
        return prod
