import logging
import os, sys

import numpy as np
import torch
from scipy import sparse
from scipy.sparse import issparse
from matplotlib import pyplot as plt
import pickle
import numpy

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
from Utils import Utils as U


class Utils(U):

    def __init__(self, path, experiment):
        super().__init__(path, experiment)
        self.result_folder = os.path.join(path, experiment, 'results')

        self.create_folder(self.result_folder)

    def load_phantom_h5(self, ids):
        f_phantom = self.load_pytable(os.path.join(self.phantom_folder, f'phantom_db.h5'))
        phantom_dim = f_phantom.root.phantoms.original[0].shape
        phantoms = np.zeros((len(ids), *phantom_dim))
        for j, i in enumerate(ids):
            idx = numpy.where(i == f_phantom.root.id.data[:])[0][0]
            phantoms[j] = f_phantom.root.phantoms.original[idx]
        f_phantom.close()
        return phantoms

    def load_sinogram_h5(self, ids):
        f_sinogram = self.load_pytable(os.path.join(self.sinogram_folder, f'sinogram_db.h5'))
        # limit ids to max available samples
        ids = [i for i in ids if i < len(f_sinogram.root.id.data)]
        idx = f_sinogram.root.id.data[ids]
        # sinograms = f_sinogram.root.sinograms.perturbed[range(min(ids), max(ids)+1)]
        sinograms = [f_sinogram.root.sinograms.perturbed[i] for i in ids]
        f_sinogram.close()
        return sinograms, idx

    def load_eta_h5(self, ids, discrete='beta'):
        f_eta = self.load_pytable(os.path.join(self.eta_folder, f'y.h5'))
        if discrete == 'beta':
            eta_obj = f_eta.root.eta_beta
        elif discrete == '':
            eta_obj = f_eta.root.eta
        else:
            raise ValueError(f'discrete {discrete} is not available.')

        eta_dim = eta_obj.data[0].shape
        etas = np.zeros((len(ids), *eta_dim))
        for j, i in enumerate(ids):
            idx = numpy.where(i == f_eta.root.id.data[:])[0][0]
            etas[j] = eta_obj.data[idx]
        f_eta.close()
        return etas

    def load_npz_weights(self, path, fov_shape, num_angles, num_detectors):
        """
        Load weights from npz file. then transform into shape (number angles, number detectors, res, res)
        :param path:
        :param fov_shape:
        :param num_angles:
        :param num_detectors:
        :return:
        """
        ray_trafo_matrix = sparse.load_npz(path)
        ray_weights = [[sparse.csr_matrix(
            ray_trafo_matrix[numpy.ravel_multi_index((i, j), (num_angles, num_detectors)),
            :].reshape(*fov_shape))
            for j in range(num_detectors)]
            for i in range(num_angles)]
        return ray_weights

    def load_npz_weights_tensor(self, path, fov_shape, num_angles, num_detectors, device='cpu'):
        """
        Load weights from npz file. then transform into shape (number angles, number detectors, res, res)
        :param path:
        :param fov_shape:
        :param num_angles:
        :param num_detectors:
        :return:
        """
        ray_trafo_matrix = sparse.load_npz(path)
        ray_weights = []
        for i in range(num_angles):
            detector_weights = []
            for j in range(num_detectors):
                sm = sparse.csr_matrix(
                    ray_trafo_matrix[numpy.ravel_multi_index((i, j), (num_angles, num_detectors)), :]
                        .reshape(*fov_shape))\
                    .tocoo()
                values = sm.data
                indices = np.vstack((sm.row, sm.col))

                ind = torch.LongTensor(indices)
                v = torch.FloatTensor(values)
                shape = sm.shape
                detector_weights.append(torch.sparse.FloatTensor(ind, v, torch.Size(shape)).to(device=device))
            ray_weights.append(detector_weights)
        return ray_weights

    def save_back_prop(self, f, suffix=''):
        """
        Save backpropagation to pickle file. Suffix to store sample id or other information.
        :param f: Backpropagated phantom
        :param suffix: str of information
        :return:
        """
        pickle.dump(f, open(os.path.join(self.result_folder, f'backpropagation_{suffix}.pkl'), 'wb'))


    @staticmethod
    def plot_two(a, b, a_name='', b_name='', close=None, title='', save=''):
        """
        Plot 2 2d arrays next to each other.
        :param a: 2d array
        :param b: 2d array
        :param a_name: title of first image
        :param b_name: title of second image
        :param close: If automatically closing, number of seconds to keep showing
        :param title: Main title of image
        :param save: If saving, path to image file location.
        :return:
        """
        if issparse(a):
            a = a.toarray()
        max_val = 1 # max(a.max(), b.max())
        fig, ax = plt.subplots(1, 2, figsize=(10, 4))
        im1 = ax[0].imshow(a, vmin=0, vmax=max_val)  #, cmap='gray')
        im2 = ax[1].imshow(b, vmin=0, vmax=max_val)  #, cmap='gray')
        ax[0].set_title(a_name, fontsize=16)
        ax[1].set_title(b_name, fontsize=16)
        # fig.colorbar(im1, ax=ax[0])
        # fig.colorbar(im2, ax=ax[1])
        cb_ax = fig.add_axes([0.93, 0.1, 0.02, 0.8])
        cbar = fig.colorbar(im1, cax=cb_ax)
        fig.suptitle(title, fontsize=18, y=1)
        # fig.subplots_adjust(top=1.2)
        # save figure
        if save:
            fig.savefig(save)
        # automatically close figure after 'close' many seconds
        if close == 0:
            return
        elif close:
            plt.show(block=False)
            plt.pause(close)
            plt.close()
        else:
            plt.show()

    def plot_errors(self, errors, close=None, name=''):
        fig, ax = plt.subplots(nrows=5, ncols=1, figsize=(12, 16))

        l0, = ax[0].plot(errors[:,0], marker='.', color='#1f77b4', label='L1')
        l1, = ax[1].plot(errors[:,1], marker='.', color='#ff7f0e', label='L2')
        l2, = ax[2].plot(errors[:,2], marker='.', color='#2ca02c', label='MSE')
        l3, = ax[3].plot(errors[:,3], marker='.', color='#d62728', label='PSNR')
        l4, = ax[4].plot(errors[:,4], marker='.', color='#9467bd', label='SSIM')

        lines = [l0, l1, l2, l3, l4]
        # ax[0].legend(lines, [l.get_label() for l in lines])

        ax[4].set_xlabel('Iterations', fontdict={'fontsize': 12})
        # ax[0].set_ylabel('error')
        #
        # ax1.grid(True)
        # ax2.grid(True)
        # ax[0].title('Error per iteration L1: {:.2f}, L2: {:.2f} MSE: {:.2f}, PSNR: {:.2f}, SSIM: {:.2f}'.format(*errors[-1]))
        ax[0].set_title('Error per iteration L1: {:.2f}'.format(errors[-1][0]), fontdict={'fontsize': 14})
        ax[1].set_title('Error per iteration L2: {:.2f}'.format(errors[-1][1]), fontdict={'fontsize': 14})
        ax[2].set_title('Error per iteration MSE: {:.2f}'.format(errors[-1][2]), fontdict={'fontsize': 14})
        ax[3].set_title('Error per iteration PSNR: {:.2f}'.format(errors[-1][3]), fontdict={'fontsize': 14})
        ax[4].set_title('Error per iteration SSIM: {:.2f}'.format(errors[-1][4]), fontdict={'fontsize': 14})
        plt.tight_layout()
        fig.savefig(os.path.join(self.result_folder, f'final_error{name}.png'))

        # automatically close figure after 'close' many seconds
        if close == 0:
            plt.close()
            return
        elif close:
            plt.show(block=False)
            plt.pause(close)
            plt.close()
        else:
            plt.show()

    def plot_errors_log(self, errors, close=None, name=''):
        fig, _ = plt.subplots()

        l1, = plt.plot(errors[:, 0], marker='.', color='#1f77b4', label='L1')
        l2, = plt.plot(errors[:, 1], marker='.', color='#ff7f0e', label='L2')
        l3, = plt.plot(errors[:, 2], marker='.', color='#2ca02c', label='MSE')
        l4, = plt.plot(errors[:, 3], marker='.', color='#d62728', label='PSNR')
        l5, = plt.plot(errors[:, 4], marker='.', color='#9467bd', label='SSIM')

        lines = [l1, l2, l3, l4, l5]
        plt.legend(lines, [l.get_label() for l in lines])

        plt.yscale('log')
        plt.xlabel('Iteration')
        plt.ylabel('L1, L2, MSE, PSNR, SSIM')
        plt.grid(True)
        plt.title(
            'Error per iteration\nL1: {:.2f}, L2: {:.2f} MSE: {:.2f}, PSNR: {:.2f}, SSIM: {:.2f}'.format(*errors[-1]))
        fig.savefig(os.path.join(self.result_folder, f'final_error_log{name}.png'))

        # automatically close figure after 'close' many seconds
        if close == 0:
            return
        elif close:
            plt.show(block=False)
            plt.pause(close)
            plt.close()
        else:
            plt.show()

