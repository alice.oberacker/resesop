import logging
import multiprocessing
import os
import pickle
import numpy
import json
from scipy import sparse
from os.path import abspath
from pathlib import Path
import tables


class Utils:

    def __init__(self, path, experiment):

        self.sinogram_folder = os.path.join(path, experiment, 'sinograms')
        self.phantom_folder = os.path.join(path, experiment, 'phantoms')
        self.weight_folder = os.path.join(path, experiment, 'weightMatrix')
        self.weight_file = "{}_angle_{}_detectors_{}_res"
        self.weight_path = os.path.join(self.weight_folder, self.weight_file)
        self.eta_folder = os.path.join(path, experiment, 'global_inexactness')
        self.perturbations_folder = os.path.join(path, experiment, 'perturbations')
        self.predictions_folder = os.path.join(path, experiment, 'predictions')
        self.model_folder = os.path.join(path, experiment, 'model_data')

    @staticmethod
    def load_pickle(path):
        """
        Load a pickled file stored in 'path'.
        :param path: File path of pickle
        :return:
        """
        try:
            return pickle.load(open(path, 'rb'))
        except FileNotFoundError as e:
            print(f'Given Path: {path}')
            print(f'Absolute Path: {abspath(path)}')
            raise e
        except Exception as e:
            raise e
            exit(1)

    @staticmethod
    def load_npz_weights(path, nb_angles, nb_detectors, fov_shape):

        try:
            # load sparse matrix
            trafo_matrix = sparse.load_npz(path)
            # re-arrange matrix into list (list (sparse matrix)) = angles x detectors x fov_shape
            weights = [
                [sparse.csr_matrix(
                    trafo_matrix[numpy.ravel_multi_index((i, j), (nb_angles, nb_detectors)), :].reshape(fov_shape))
                 for j in range(nb_detectors)]
                for i in range(nb_angles)]
            return weights
        except FileNotFoundError as e:
            print(f'Given Path: {path}')
            print(f'Absolute Path: {abspath(path)}')
            raise e
        except Exception as e:
            raise e
            exit(1)

    @staticmethod
    def load_pytable(path, mode='r'):
        """
        Load pytable object from path in mode.
        Must be closed after calling this function (f.close())
        :param path: path of pytable h5 file
        :param mode: opening mode, default read
        :return:
        """
        filter = tables.Filters(complib='zlib', complevel=5)
        f = tables.open_file(path, mode=mode, filters=filter)
        return f

    @staticmethod
    def load_json(path):
        """
        Load JSON object from file. If file does not exist return empty object.
        :param path: File path of json
        """
        j = {}
        try:
            with open(path, 'r') as fd:
                j = json.load(fd)
        except FileNotFoundError:
            # file does not exist yet
            pass
        except Exception as e:
            raise e

        return j

    @staticmethod
    def create_folder(path):
        """
        Create all folders for this experiment. If they already exist, ignore errors.
        If parent folders don't exist, create them.
        :return:
        """
        Path(path).mkdir(parents=True, exist_ok=True)

    @staticmethod
    def gen_batch_id_index(n, batch_size, wanted_index=None):
        """
        Generator to return start and end index of a batch with size batch_size
        :param n: Maximum
        :param batch_size: Size of batch
        :return:
        """
        if wanted_index is None:
            i = 0
            while i < n:
                if i + batch_size < n:
                    yield i, i + batch_size - 1
                else:
                    yield i, n - 1
                i += batch_size
        else:
            i = 0
            while i < n:
                if i + batch_size < n:
                    if i <= wanted_index <= i + batch_size - 1:
                        yield i, i + batch_size - 1
                else:
                    if i <= wanted_index <= n - 1:
                        yield i, n - 1
                i += batch_size

    @staticmethod
    def weights_generator(id_generator, weight_file):
        """
        This is a generator to load the pickled weights in batches.
        Only one weight file is loaded and returned at a time.
        :return: Numpy array of weights.
        """
        # generate path with weight folder, experiment setup and batch size
        for (b1, b2) in id_generator:
            path = weight_file.format(b1, b2)
            # load batch of pickled weights
            angle_ray_weights = Utils.load_pickle(path)
            # self.sinogram_batch_shape = (len(self.angle_ray_weights[0]), len(self.angle_ray_weights))
            yield angle_ray_weights, b1, b2

    def set_up_logging(self, level='info', filename='', with_stdout=True):
        """
        Set up logging class.

        :param level: Defines which level of Logger to call.
        :return: logging object, multiprocessing lock
        """
        logger = logging.getLogger(f'{filename}_{level}')
        if len(logger.handlers) == 0:
            formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(message)s')
            if level == 'info':
                logger.setLevel(logging.INFO)
            elif level == 'debug':
                logger.setLevel(logging.DEBUG)
            # define file handler and set formatter
            file_handler = logging.FileHandler(os.path.join(self.result_folder, f'resesop_{level}.log'))
            file_handler.setFormatter(formatter)
            # add file handler to log_debug
            logger.addHandler(file_handler)
            if with_stdout:
                consoleHandler = logging.StreamHandler()
                consoleHandler.setFormatter(formatter)
                logger.addHandler(consoleHandler)

        return logger
