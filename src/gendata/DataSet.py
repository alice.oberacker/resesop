import numpy
import tables
from torch.utils.data import Dataset


class MyResesopDataset(Dataset):
    def __init__(self, sino_shape, file_paths={'sinograms': '', 'eta': '', 'phantoms': ''}, dataset_name='train'):
        super().__init__()
        # phantoms should be optional, only for plotting
        assert 'sinograms' and 'eta' in file_paths.keys()
        self.file_paths = file_paths
        self.dataset_name = dataset_name
        self.length = None
        self.zero_eta = True if file_paths['eta'][-4:] == 'zero' else False
        self.sino_shape = sino_shape

        filter = tables.Filters(complib='zlib', complevel=5)
        with tables.open_file(self.file_paths['sinograms'], mode='r', filters=filter) as sino_f:
            self.length = len(sino_f.root.X[dataset_name])
            # get sorted index, necessary so different datasets produce output in same order
            self.sorted_index = sorted(sino_f.root.id[dataset_name])

    def __len__(self):
        assert self.length is not None
        return self.length

    def _open_hdf5(self):
        filter = tables.Filters(complib='zlib', complevel=5)
        self.sinograms_hf = tables.open_file(self.file_paths['sinograms'], mode='r', filters=filter)
        if not self.zero_eta:
            self.eta_hf = tables.open_file(self.file_paths['eta'], mode='r', filters=filter)
        if self.file_paths['phantoms'] is not None:
            self.phantoms_hf = tables.open_file(self.file_paths['phantoms'], mode='r', filters=filter)

    def __getitem__(self, index):
        if not hasattr(self, 'sinograms_hf'):
            self._open_hdf5()
        # get index from sorted sinogram indices
        id = self.sorted_index[index]
        sino_idx = numpy.where(self.sinograms_hf.root.id[self.dataset_name][:] == id)[0][0]
        # get sinogram value
        sinogram = self.sinograms_hf.root.X[self.dataset_name][sino_idx]
        # reshape sinogram, because it's square
        sinogram = sinogram[0, :self.sino_shape[0], :self.sino_shape[1]]
        # # get index of this value
        # id = self.sinograms_hf.root.id[self.dataset_name][index]
        # get eta value for that index
        if self.zero_eta:
            # eta = numpy.random.uniform(low=0, high=0.0001, size=sinogram.shape)
            eta = numpy.zeros(sinogram.shape)
        else:
            eta_idx = numpy.where(self.eta_hf.root.id.data[:] == id)[0][0]
            eta = self.eta_hf.root.eta_beta_detector.data[eta_idx]

        # if needed get phantom value
        if self.file_paths['phantoms'] is not None:
            phantom_idx = numpy.where(self.phantoms_hf.root.id.data[:] == id)[0][0]
            phantom = self.phantoms_hf.root.phantoms.original[phantom_idx]
        else:
            phantom = None

        return sinogram, eta, id, phantom

    def close_files(self):
        try:
            self.sinograms_hf.close()
            if hasattr(self, 'eta_hf'):
                self.eta_hf.close()
            if hasattr(self, 'phantoms_hf'):
                self.phantoms_hf.close()
        except AttributeError:
            print("Can't find sinograms_hf and eta_hf to close.")
            pass
