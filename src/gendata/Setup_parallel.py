import numpy


class SetupParallel:

    def __init__(self, experiment_id='5', save_all=False, transf_index_space=True):

        #####################
        # Folder SETUP ##
        #####################
        self.EXPERIMENT = 'experiment#' + experiment_id
        self.SAVE_ALL = save_all
        self.TRANSFORM_INDEX_SPACE = transf_index_space

        ##################
        # Phantom SETUP ##
        ##################

        # more parameters for phantom and features
        self.PH_NAME = "original_phantom_"
        self.PH_PERTURBED_NAME = "sequence_perturbed_"
        self.SINO_NAME = "original_sinogram_"
        self.SINO_PERTURBED_NAME = "sinogram_perturbed_"
        self.PHANTOM_SETTINGS = {#'number_features': self.NUMBER_FEATURES,
                                 'center_area_ph': 0.1,
                                 'center_area_ft': 0.3,
                                 'size_range_ph': (0.4, 0.8),
                                 'size_range_ft': (0.1, 0.2),
                                 'shapes_ph': {"rectangle": 0.3, "ellipse": 0.7},
                                 'shapes_ft': {"rectangle": 0.3, "ellipse": 0.7},
                                 'graylevels_ph': (0.2, 0.8),
                                 'graylevels_ft': (0.2, 0.8)
                                 }
        self.DET_POINTS_ON_BOUNDARY = False

        #####################
        # Experiment SETUP ##
        #####################
        if self.EXPERIMENT == 'experiment#1':
            # Batch settings
            self.BATCH_SIZE = 40
            # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 127
            # Number of scanner positions (# phi) K:
            self.K = 567
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 181

            phi_step = 180/self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * phi_step, 180 - 0.5 * phi_step, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = 0.12  # 20/161 ## 0.12
            # if sinus noise should be created
            self.SINUS = True
            # random x,y translations
            self.TRANSLATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # random rotation
            self.ROTATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # how many sinus waves to generate
            self.WAVE_DENSITY = 1 / 15

            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                'opposite_signs': True,
                'random': {
                    'rotation': 0.001 * self.R, # scale in truncnorm
                    'shear': 0 * self.R,
                    'translation': (0.001 * self.R, 0.001 * self.R) # scale in truncnorm
                },
                'sinus': {
                    'number_waves': round((self.K - 1) * 1 / 15),
                    'amplitude': lambda: (yield numpy.random.normal(0.062, 0.01) * self.R),
                    'amplitude_r': lambda: (yield numpy.random.uniform(0, 1)),
                    'frequency': lambda: (yield numpy.random.uniform(0.05, 0.09)),
                    'dampening': lambda: (yield numpy.random.uniform(0.01, 0.1))
                },
            }
            # number of features
            self.NUMBER_FEATURES = 3
        elif self.EXPERIMENT == 'experiment#01':
            # Batch settings
            self.BATCH_SIZE = 40
            # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 127
            # Number of scanner positions (# phi) K:
            self.K = 567
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 181

            phi_step = 180/self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * phi_step, 180 - 0.5 * phi_step, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = 0.12  # 20/161 ## 0.12
            # if sinus noise should be created
            self.SINUS = True
            # random x,y translations
            self.TRANSLATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # random rotation
            self.ROTATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # how many sinus waves to generate
            self.WAVE_DENSITY = 1 / 15

            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                'opposite_signs': True,
                'random': {
                    'rotation': 0.001 * self.R, # scale in truncnorm
                    'shear': 0 * self.R,
                    'translation': (0.001 * self.R, 0.001 * self.R) # scale in truncnorm
                },
                'sinus': {
                    'number_waves': round((self.K - 1) * 1 / 15),
                    'amplitude': lambda: (yield numpy.random.normal(0.062, 0.01) * self.R),
                    'amplitude_r': lambda: (yield numpy.random.uniform(0, 1)),
                    'frequency': lambda: (yield numpy.random.uniform(0.05, 0.09)),
                    'dampening': lambda: (yield numpy.random.uniform(0.01, 0.1))
                },
            }
            # number of features
            self.NUMBER_FEATURES = 3
        elif self.EXPERIMENT == 'experiment#2':
            # # Batch settings
            # self.BATCH_SIZE = 8
            # # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 25
            # Number of scanner positions (# phi) K:
            self.K = 111
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 36

            self.ANGLES = numpy.linspace(0, 180, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = (1281 / 322) * (20 / 322)  # (1281pxl / 322μm) * (20μm / 322μm)
            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                'opposite_signs': True,
                'linear': {
                    'rotation': 0.001 * self.R,
                    'shear': 0,
                    'translation': ((self.MAX_SHIFT * self.R) / (self.K - 1), (self.MAX_SHIFT * self.R) / (self.K - 1))
                }
            }

            # number of features
            self.NUMBER_FEATURES = 0

            self.PHANTOM_SETTINGS = {'center_area_ph': 0.1,
                                     'center_area_ft': 0.3,
                                     'size_range_ph': (0.4, 0.8),
                                     'size_range_ft': (0.1, 0.2),
                                     'shapes_ph': {"ellipse": 1},
                                     'shapes_ft': {},
                                     'graylevels_ph': (0.2, 0.8),
                                     'graylevels_ft': (0.2, 0.8)
                                     }

        elif self.EXPERIMENT == 'experiment#4':
            # Batch settings
            self.BATCH_SIZE = 8
            # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 40
            # Number of scanner positions (# phi) K:
            self.K = 178
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 57

            self.ANGLES = numpy.linspace(0, 180, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = (1281 / 322) * (20 / 322)  # (1281pxl / 322μm) * (20μm / 322μm)
            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                'opposite_signs': True,
                'linear': {
                    'rotation': 0,
                    'shear': 0,
                    'translation': ((self.MAX_SHIFT * self.R) / (self.K - 1), (self.MAX_SHIFT * self.R) / (self.K - 1))
                }
            }

            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#5':
            # Batch settings
            self.BATCH_SIZE = 40
            # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 127
            # Number of scanner positions (# phi) K:
            self.K = 567
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 181

            phi_step = 180 / self.K
            self.ANGLES = numpy.linspace(0 + 0.5 * phi_step, 180 - 0.5 * phi_step, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = 0.12  # 20/161 ## 0.12
            # if sinus noise should be created
            self.SINUS = True
            # random x,y translations
            self.TRANSLATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # random rotation
            self.ROTATION = lambda: (yield numpy.random.normal(0.001, 0.0002))  # 0.001
            # how many sinus waves to generate
            self.WAVE_DENSITY = 1 / 15

            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                'worst': {
                },
            }
            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#6':
            # Batch settings
            self.BATCH_SIZE = 8
            # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 40
            # Number of scanner positions (# phi) K:
            self.K = 178
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 57

            self.ANGLES = numpy.linspace(0, 180, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = (1281 / 322) * (20 / 322)  # (1281pxl / 322μm) * (20μm / 322μm)
            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                # 'sinus': {
                #     'number_waves': round((self.K - 1) * 1 / 15),
                #     'amplitude': lambda: (yield numpy.random.normal(0.062, 0.01) * self.R),
                #     'amplitude_r': lambda: (yield numpy.random.uniform(0, 1)),
                #     'frequency': lambda: (yield numpy.random.uniform(0.05, 0.09)),
                #     'dampening': lambda: (yield numpy.random.uniform(0.01, 0.1)),
                #     'translation': (
                #         lambda: (yield numpy.random.normal(0.1, 0.002)),
                #         lambda: (yield numpy.random.normal(0.1, 0.002))),  # 0.001
                #     'rotation': lambda: (yield numpy.random.normal(0.001, 0.0002))
                # },
                # 'random': {
                #     'rotation': 0.01, # scale in truncnorm
                #     'shear': 0,
                #     'translation': (0.1, 0.1) # scale in truncnorm
                # },
                'linear': {
                    'rotation': 0.005,
                    'shear': 0,
                    'translation': ((self.MAX_SHIFT * self.R) / (self.K - 1), (self.MAX_SHIFT * self.R) / (self.K - 1))
                }
            }

            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#7':
            # Batch settings
            self.BATCH_SIZE = 8
            # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 20
            # Number of scanner positions (# phi) K:
            self.K = 90
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 29

            self.ANGLES = numpy.linspace(0, 180, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = (1281 / 322) * (20 / 322)  # (1281pxl / 322μm) * (20μm / 322μm)
            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                # 'sinus': {
                #     'number_waves': round((self.K - 1) * 1 / 15),
                #     'amplitude': lambda: (yield numpy.random.normal(0.062, 0.01) * self.R),
                #     'amplitude_r': lambda: (yield numpy.random.uniform(0, 1)),
                #     'frequency': lambda: (yield numpy.random.uniform(0.05, 0.09)),
                #     'dampening': lambda: (yield numpy.random.uniform(0.01, 0.1)),
                #     'translation': (
                #         lambda: (yield numpy.random.normal(0.1, 0.002)),
                #         lambda: (yield numpy.random.normal(0.1, 0.002))),  # 0.001
                #     'rotation': lambda: (yield numpy.random.normal(0.001, 0.0002))
                # },
                'random': {
                    'rotation': 0.01, # scale in truncnorm
                    'shear': 0,
                    'translation': (0.2, 0.2) # scale in truncnorm
                },
                'linear': {
                    'rotation': 0,
                    'shear': 0,
                    'translation': ((self.MAX_SHIFT * self.R) / (self.K - 1), (self.MAX_SHIFT * self.R) / (self.K - 1))
                }
            }

            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#8':
            # Batch settings
            self.BATCH_SIZE = 8
            # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 20
            # Number of scanner positions (# phi) K:
            self.K = 90
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 29

            self.ANGLES = numpy.linspace(0, 180, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = (1281 / 322) * (20 / 322)  # (1281pxl / 322μm) * (20μm / 322μm)
            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                # 'sinus': {
                #     'number_waves': round((self.K - 1) * 1 / 15),
                #     'amplitude': lambda: (yield numpy.random.normal(0.062, 0.01) * self.R),
                #     'amplitude_r': lambda: (yield numpy.random.uniform(0, 1)),
                #     'frequency': lambda: (yield numpy.random.uniform(0.05, 0.09)),
                #     'dampening': lambda: (yield numpy.random.uniform(0.01, 0.1)),
                #     'translation': (
                #         lambda: (yield numpy.random.normal(0.1, 0.002)),
                #         lambda: (yield numpy.random.normal(0.1, 0.002))),  # 0.001
                #     'rotation': lambda: (yield numpy.random.normal(0.001, 0.0002))
                # },
                'random': {
                    'rotation': 0.01, # scale in truncnorm
                    'shear': 0,
                    'translation': (0.5, 0.5) # scale in truncnorm
                },
                # 'linear': {
                #     'rotation': 0,
                #     'shear': 0,
                #     'translation': ((self.MAX_SHIFT * self.R) / (self.K - 1), (self.MAX_SHIFT * self.R) / (self.K - 1))
                # }
            }

            # number of features
            self.NUMBER_FEATURES = 3

        elif self.EXPERIMENT == 'experiment#06':
            # Batch settings
            self.BATCH_SIZE = 8
            # self.IN_BATCHES = True
            self.ASTRA = True
            # EXPERIMENT SETUP
            # pixel number of field of view (resolution) ((2R + 1) × (2R + 1)), R:
            self.R = 40
            # Number of scanner positions (# phi) K:
            self.K = 178
            # Number of detector points (number_detectors) (2P + 1), P:
            self.P = 57

            self.ANGLES = numpy.linspace(0, 180, self.K) - 90

            # NOISE
            # maximum shift of object
            self.MAX_SHIFT = (1281 / 322) * (20 / 322)  # (1281pxl / 322μm) * (20μm / 322μm)
            # random x,y translations, rotations
            self.MOVEMENT = {
                'number_transforms': self.K - 1,
                'total_max_shift': self.MAX_SHIFT * self.R,
                # 'sinus': {
                #     'number_waves': round((self.K - 1) * 1 / 15),
                #     'amplitude': lambda: (yield numpy.random.normal(0.062, 0.01) * self.R),
                #     'amplitude_r': lambda: (yield numpy.random.uniform(0, 1)),
                #     'frequency': lambda: (yield numpy.random.uniform(0.05, 0.09)),
                #     'dampening': lambda: (yield numpy.random.uniform(0.01, 0.1)),
                #     'translation': (
                #         lambda: (yield numpy.random.normal(0.1, 0.002)),
                #         lambda: (yield numpy.random.normal(0.1, 0.002))),  # 0.001
                #     'rotation': lambda: (yield numpy.random.normal(0.001, 0.0002))
                # },
                # 'random': {
                #     'rotation': 0.01, # scale in truncnorm
                #     'shear': 0,
                #     'translation': (0.1, 0.1) # scale in truncnorm
                # },
                'linear': {
                    'rotation': 0.005,
                    'shear': 0,
                    'translation': ((self.MAX_SHIFT * self.R) / (self.K - 1), (self.MAX_SHIFT * self.R) / (self.K - 1))
                }
            }

            # number of features
            self.NUMBER_FEATURES = 3

        self.PHANTOM_SETTINGS['number_features'] = self.NUMBER_FEATURES

