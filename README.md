# RESESOP
# Regularised Sequential Subspace Optimisation

***
# Description
This repository provides an implementation of RESESOP-Kaczmarz following the algorithm provided in [[1]](#1).
You can find the pseudocode at the bottom of this README or in this paper [[2]](#2).

This algorithm has been applied to a synthetic dataset in conjunction with learned post-processing methods. You can view the results here [[2]](#2).

# How to run
## Installation
There are two options to install the python packages provided: a singularity image or anaconda environments.
### Singularity
A definition file has been provided in install/singularity.def. With this .def file you can create a Singularity image that installs all necessary packages in an anaconda environment. From this file you could also extract the install commands for an Anaconda enviroment without the intermediate step of using Singularity. 

It is also possible to download the singularity image with this command:
`singularity pull library://aoberac/nanoct/gendata`
### Anaconda
There is a yml file provided to install the environment to run RESESOP (resesop.yml).

## Run Script
To execute RESESOP-Kaczmarz run **run_resesop.py**.
This script loads sinograms from sinograms/sinogram_db.h5 and the weights from weightMatrix 
### Folder structure
To execute RESESOP the following folder structure is expected:

![Folder structure](img/folder_structure.png)

Phantoms are used for error calculations and plotting. The results are stored as pickle files in the results folder.
### run_resesop
#### Parameters
- path: Path of data directory
- experiment_id: Id of experiment, geometry settings defined in gendata/Setup_parallel.py or gendata/Setup_fanflat.py 
- device: use cuda or cpu
- geometry: parallel or fan
- cores: Number of cores to use
- eta: Use calculated eta or zero eta
- iterations: Number of iterations to execute
- samples: Number of samples to process 
- start_id: First sample id to run in dataset

#### Results
This script generates pickle files for each sample containing the backprojected phantom data.
These pickle files are stored in the results folder.

# Data
This code is best applied to a synthetic dataset generated usign [this repository](https://gitlab.gwdg.de/alice.oberacker/generate-data-4-nanoct) or by downloading the dataset [here](https://zenodo.org/record/8123499).

## Structure of data files
The code expects a data file in the HDF5 format ([PyTables](https://www.pytables.org/usersguide/file_format.html)).
The base level is root ('/') followed by id/data and sinograms/perturbed.

Example structure for sinogram file:
#### Sinogram
![Sinogram](img/sinogram_h5.png)



# Pseudocode for RESESOP [[2]](#2)
![Pseudocode](img/pseudocode.png)

## Authors and acknowledgment
The code in src/raytrafo has mostly been developed by Johannes Leuschner from University of Bremen.
Alice Oberacker is the owner of this repository and has written everything else.
For questions please contact [Alice Oberacker](mailto:alice.oberacker@num.uni-sb.de).

## References
<a id="1">[1]</a> 
Blanke, S.E., Hahn, B.N. and Wald, A., 2020. Inverse problems with inexact forward operator: iterative regularization and application in dynamic imaging. Inverse Problems, 36(12), p.124001.

<a id="2">[2]</a> 
Lütjen, T., Schönfeld, F., Oberacker, A., Leuschner, J., Schmidt, M., Wald, A. and Kluth, T., 2023. Learning-based approaches for reconstructions with inexact operators in nanoCT applications. arXiv preprint arXiv:2307.10474.
https://arxiv.org/abs/2307.10474

